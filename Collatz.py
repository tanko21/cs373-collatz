#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, Tuple

# ------------
# collatz_read
# ------------

def collatz_read (s: str) -> Tuple[int, int] :
    """
    read two ints
    s a string
    return a tuple of two ints
    """
    a = s.split()
    return int(a[0]), int(a[1])

# ------------
# collatz_eval
# ------------

def collatz_eval (t: Tuple[int, int]) -> Tuple[int, int, int] :
    """
    t a tuple of two ints
    return a tuple of three ints
    """
    i, j = t
    # <your code>
    return i, j, i + j

# -------------
# collatz_print
# -------------

def collatz_print (sout: IO[str], t: Tuple[int, int, int]) -> None :
    """
    print three ints
    sout a writer
    t a tuple of three ints
    """
    i, j, v = t
    sout.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------

def collatz_solve (sin: IO[str], sout: IO[str]) -> None :
    """
    sin  a reader
    sout a writer
    """
    for s in sin :
        collatz_print(sout, collatz_eval(collatz_read(s)))
